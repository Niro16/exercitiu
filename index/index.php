<html>
<head>
        <title> User login and registration </title>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <div class="login-box">
    <div class="row">
    <div class="col-md-6 login">
        <h2> Login </h2>
        <form action="validation.php" method="post">
            <div class="form-group">
            <label>Username</labe>
            <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
            <label>Password</labe>
            <input type="password" name="password" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary" > Login </button>

        </form>
    </div>

    <div class="col-md-6 register">
        <h2> Register </h2>
        <form action="registration.php" method="post">
            <div class="form-group">
            <label>Username</labe>
            <input type="text" name="name" class="form-control" required>
            </div>
            <div class="form-group">
            <label>Email</labe>
            <input type="email" name="email" class="form-control" required>
            </div>
            <div class="form-group">
            <label>Password</labe>
            <input type="password" name="password" class="form-control" required>
            </div>
            <button type="submit" class="btn btn-primary" > Register </button>

        </form>
    </div>

    </div>
       
    </div>



</body>
</html>